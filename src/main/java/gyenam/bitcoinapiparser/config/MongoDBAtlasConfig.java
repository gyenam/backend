package gyenam.bitcoinapiparser.config;

import com.mongodb.connection.SslSettings;
import com.mongodb.connection.netty.NettyStreamFactoryFactory;
import io.netty.channel.nio.NioEventLoopGroup;
import org.springframework.boot.autoconfigure.mongo.MongoClientSettingsBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;

/**
 * Package : gyenam.bitcoinapiparser.config
 * Developer Team : Mrblue WebPlatform Developer Team
 * Developer: wyparks2
 * Date : 2018. 1. 17.
 * Time : PM 10:03
 * Created by IntelliJ IDEA.
 *
 * @http://www.solidsyntax.be/2017/11/08/Configure-Spring-Boot-to-connect-to-MongoDB-Atlas-with-SSL/
 */
@Configuration
public class MongoDBAtlasConfig {
    private NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();

    @Bean
    public MongoClientSettingsBuilderCustomizer sslCustomizer() {
        return clientSettingsBuilder -> clientSettingsBuilder
            .sslSettings(SslSettings.builder()
                .enabled(true)
                .invalidHostNameAllowed(true)
                .build())
            .streamFactoryFactory(NettyStreamFactoryFactory.builder()
                .eventLoopGroup(eventLoopGroup).build());
    }
    @PreDestroy
    public void shutDownEventLoopGroup() {
        eventLoopGroup.shutdownGracefully();
    }
}
