package gyenam.bitcoinapiparser.common.util;

import com.mongodb.connection.Server;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import javax.validation.Validator;

/**
 * Package : gyenam.bitcoinapiparser.common.util
 * Developer Team : Mrblue WebPlatform Developer Team
 * Developer: wyparks2
 * Date : 2018. 1. 17.
 * Time : PM 10:20
 * Created by IntelliJ IDEA.
 */
@Slf4j
@Component
public class ValidationUtil {
    private static Validator validator;

    @Autowired
    public ValidationUtil(Validator validator) {
        this.validator = validator;
    }

    /**
     * Javax.validator를 이용하여 필수 값, 널 체크, 최소값, 최대값, 문자열 길이 등 검증 처리를 수행한다.
     * 검증 통과 성공 : 파라미터로 전달된 ServerResposne 객체를 반환한다.
     * 검증 통과 실패 : 검증관련 오류 메시지 출력 및 400 에러 코드를 응답한다.
     * @param t 검증할 모델
     * @param callback 검증 통과 시 반활할 ServerResponse 객체
     * @return
     */
    public static <T> Mono<ServerResponse> validationAndResponse(T t, Mono<ServerResponse> callback) {
        return validator.validate(t).stream()
            .findFirst()
            .map(v ->
                ServerResponse.badRequest().syncBody(v.getMessage())
            ).orElse(callback);
    }

}
